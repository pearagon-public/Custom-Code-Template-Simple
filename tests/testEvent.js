exports.event = {
    "origin": {
        // Your portal ID
        "portalId": 1,

        // Your custom action definition ID
        "actionDefinitionId": 2,
    },
    "object": {
        // The type of CRM object that is enrolled in the workflow
        "objectType": "DEAL",

        // The ID of the CRM object that is enrolled in the workflow

        //-----------------------------------------------------------------
        "objectId": 7186080945,
        //-----------------------------------------------------------------
    },

    //Set input fields https://developers.hubspot.com/docs/api/workflows/custom-code-actions#add-hubspot-properties-to-your-custom-code
    "inputFields": {
        // The property name for defined inputs
        "email": "info@pearagon.com",
        "firstname": "Perry",
        "lastname": "Pearagon",
        "phone": "571-5asdasdlkajsdlkajdsl21"
    },
    // A unique ID for this execution
    "callbackId": "ap-123-456-7-8"  //Generally not used
}