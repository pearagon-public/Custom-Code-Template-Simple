require('dotenv').config()

//use: CustomCodeTester.js action contactTestEvent
const customCode = require("../" + process.argv[2]);
const { event } = require("../" + process.argv[3]);

customCode.main(event, (output) => {
    console.log(output);
});