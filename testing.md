# Help
[Duplicate a repo tutorial](https://docs.github.com/en/repositories/creating-and-managing-repositories/duplicating-a-repository)

# Perquisites 

* node 16.x and npm

# Setup

1. Copy the env-template to a .env file and set any needed environmental variables and add new ones if needed
1. Modify the event inside [contactTestEvent.js](./tests/contactTestEvent.js)
1. run `npm install`

# Testing
* To test your custom code action run `npm start` 
* To add test a specific test case against a specific action run `node tests/CustomCodeTester.js src/action tests/contactTestEvent`

# Building
Only do this step if you included npm modules that were not available [here](https://developers.hubspot.com/docs/api/workflows/custom-code-actions).
1. run `npm build`
1. By default this will build `action.js` to `out.js`
1. `out.js` can just be copied and pasted into the custom code action(this will include all external libraries)

View the build script in [package.json] to modify the name of the action file and to include more external libraries if this repository gets out of date from the documentation.

# Moving to a real workflow
1. Make sure you include needed secrets
1. Configure your outputs
1. Configure your input fields
1. Copy your code directly from code.js to the custom code action
