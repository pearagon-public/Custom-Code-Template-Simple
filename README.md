This repository contains a custom code action and the means of testing and building said action.

For more details please view the code action itself at 
### [action.js](./action.js)
 
or how to test at [testing.md](./testing.md)
