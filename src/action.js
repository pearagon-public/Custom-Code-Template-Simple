/**
 * 
 *
 * @link   
 * @author Pearagon LLC,
 * @since  -/--/2022
 */
const axios = require('axios').default;

exports.main = async (event, callback) => {
    try {
        const client =  axios.create({
            baseURL: 'https://api.hubapi.com',
            headers: { 
              accept: 'application/json', 
              Authorization: `Bearer ${process.env.APPTOKEN}`
            }
        });
        
        const {data: associationData} = await client.get(`/crm/v3/objects/${event.object.objectType}/${event.object.objectId}?associations=company,deal`);
      	const associatedCompanyIds = associationData.associations.companies ? associationData.associations.companies.results.map( x => x.id) : []
        const associatedDealIds = associationData.associations.deals ? associationData.associations.companies.results.map( x => x.id) : []
        console.log(JSON.stringify(associationData, null, 2));
		
      
        return callback({
            outputFields: {associatedCompanyIds: associatedCompanyIds.join(","), dealIds: associatedDealIds.join(",")}
        });

    } catch (error) {
        console.log(error.message)
        if (error.config) {
            //The actual request that you are making
            console.log("config baseURL:", error.config.baseURL)
            console.log("config method:", error.config.method)
            console.log("config url:", error.config.url)
            console.log("config data:", error.config.data)
        }
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log("response data:", error.response.data);
            console.log("response status:", error.response.status);
            //Generally the headers are not needed
            //console.log(error.response.headers);
        } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log("request:", error.request);
        }
        throw "Failure"
    }
}

/* A sample event may look like:
{
  "origin": {
    // Your portal ID
    "portalId": 1,

    // Your custom action definition ID
    "actionDefinitionId": 2,
  },
  "object": {
    // The type of CRM object that is enrolled in the workflow
    "objectType": "CONTACT",

    // The ID of the CRM object that is enrolled in the workflow
    "objectId": 4,
  },
  "inputFields": {
    // The property name for defined inputs
  },
  // A unique ID for this execution
  "callbackId": "ap-123-456-7-8"
}
*/
